# generation-example
> An experiment for combining the results of multiple code generation tools into one project

## Generation Tools

* [swaggo/swag](https://github.com/swaggo/swag) for swagger documentation
* [di](https://gitlab.com/cosban/di) for dependency injection

## Other Tools / Libs
* [endpoints](https://gitlab.com/cosban/endpoints) for wrapping of http endpoints
* [gorilla](https://github.com/gorilla/mux) a request router and dispatcher
* [gin](https://github.com/gin-gonic/gin) a web framework

## Generating code

1. Everything except swagger can be generated via 
```
go generate ./...
```
2. To generate swagger documentation, ensure swag is installed and run
```
swag init -g resources/server.go
```

## Running

```
go run main.go
```

Then navigate to `localhost:8080/swagger/` in a browser
