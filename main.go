package main

import (
	_ "gitlab.com/cosban/generation-example/docs"
	"gitlab.com/cosban/generation-example/module"
)

// Serve godoc
// @title generation-example
// @version 1.0
// @description just an example
func main() {
	mod := &module.ModuleComponent{}
	mod.GorillaHttpServer().ListenAndServe()
	defer mod.GorillaHttpServer().Close()
	// OR
	mod.GinHttpServer().ListenAndServe()
	defer mod.GinHttpServer().Close()

}
