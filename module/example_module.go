//go:generate di example_module.go
package module

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/cosban/endpoints"
	"gitlab.com/cosban/generation-example/resources"
	"net/http"
)

func Path() string {
	return "localhost"
}

func Port() int {
	return 8080
}

func Address(path string, port int) string {
	return fmt.Sprintf("%s:%d", path, port)
}

// @singleton
func GinServer() endpoints.Server[gin.HandlerFunc] {
	server := endpoints.GinServer()
	server.Register("/example", http.MethodPost, endpoints.GinJSONHandler(resources.Example))
	return server
}

// @singleton
func GorillaServer() endpoints.Server[http.HandlerFunc] {
	server := endpoints.GorillaServer()
	server.Register("/example", http.MethodPost, endpoints.GorillaJSONHandler(resources.Example))
	return server
}

// @singleton
func GinHttpServer(address string, ginServer endpoints.Server[gin.HandlerFunc]) *http.Server {
	return &http.Server{
		Addr:    address,
		Handler: ginServer.Handler(),
	}
}

// @singleton
func GorillaHttpServer(address string, gorillaServer endpoints.Server[http.HandlerFunc]) *http.Server {
	return &http.Server{
		Addr:    address,
		Handler: gorillaServer.Handler(),
	}
}
