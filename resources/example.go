package resources

import (
	"context"

	"github.com/google/uuid"
)

type ExampleRequestBody struct {
	Users []uuid.UUID `json:"users"`
}

type ResponseBody struct {
	Message string `json:"message"`
}

// Example godoc
// @summary an example endpoint
// @description this endpoint does nothing
// @accept json
// @produce json
// @param exampleParam body ExampleRequestBody true "list of users"
// @success 200 {object} ResponseBody
// @failure 400
// @name Example
// @router /example [post]
func Example(context context.Context, request ExampleRequestBody) (ResponseBody, error) {
	return ResponseBody{}, nil
}
